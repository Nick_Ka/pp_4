import os


def what_encoding(file_name):
    from chardet.universaldetector import UniversalDetector
    detector = UniversalDetector()
    with open(file_name, 'rb') as file:
        for line in file:
            detector.feed(line)
            if detector.done:
                print(f'detector = {detector.result}')
                break
        detector.close()
    return detector.result


def run(file_name):
    file_name = os.path.abspath(file_name)
    print(what_encoding(file_name=file_name)['encoding'])
    with open(file_name, 'r', encoding=what_encoding(file_name=file_name)['encoding']) as file:
        for line in file:
            new_file_name = f'{file_name}_UTF8.txt'
            with open(new_file_name, 'a', encoding='utf8') as new_file:
                print(line)
                new_file.write(line)



run('/home/nick_ka/Загрузки/ФНС/Какая-то хрень.txt')


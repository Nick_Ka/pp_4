# -*- coding: utf-8 -*-

import os, time, shutil

# Нужно написать скрипт для упорядочивания фотографий (вообще любых файлов)
# Скрипт должен разложить файлы из одной папки по годам и месяцам в другую.
# Например, так:
#   исходная папка
#       icons/cat.jpg
#       icons/man.jpg
#       icons/new_year_01.jpg
#   результирующая папка
#       icons_by_year/2018/05/cat.jpg
#       icons_by_year/2018/05/man.jpg
#       icons_by_year/2017/12/new_year_01.jpg
#
# Входные параметры основной функции: папка для сканирования, целевая папка.
# Имена файлов в процессе работы скрипта не менять, год и месяц взять из времени создания файла.
# Обработчик файлов делать в обьектном стиле - на классах.
#
# Файлы для работы взять из архива icons.zip - раззиповать проводником в папку icons перед написанием кода.
# Имя целевой папки - icons_by_year (тогда она не попадет в коммит)
#
# Пригодятся функции:
#   os.walk
#   os.path.dirname
#   os.path.join
#   os.path.normpath
#   os.path.getmtime
#   time.gmtime
#   os.makedirs
#   shutil.copy2
#
# Чтение документации/гугла по функциям - приветствуется. Как и поиск альтернативных вариантов :)
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.
import zipfile
from pprint import pprint


class Sorter:
    def __init__(self):
        self.input_folder = None
        self.output_folder = None
        self.file_name = ''

    def sort(self, input_folder, output_folder):
        self.input_folder = input_folder
        self.output_folder = os.path.join(os.path.dirname(input_folder), output_folder)

        for dirpath, dirnames, filenames in os.walk(self.input_folder):
            for file in filenames:
                full_file_path = os.path.join(dirpath, file)

                old_folder = os.path.join(dirpath, file)
                new_folder = os.path.join(self._set_new_folder(full_file_path=full_file_path), file)
                os.replace(old_folder, new_folder)

    # так ее и недобил :(
    def unzip_and_sort(self, zip_file_name, output_folder):
        self.output_folder = output_folder
        os.chdir(path=os.path.dirname(zip_file_name))
        root_dir = os.path.dirname(zip_file_name)

        self.zip_file_name = zip_file_name
        zfile = zipfile.ZipFile(self.zip_file_name, 'r')
        # pprint(zfile.namelist())
        for filename in zfile.namelist():
            jast_name = os.path.basename(filename)

            # print(f'что-суем-в-функцию  {root_dir} ')
            # разпаковываем в временную папку "temp"
            temp_file = zfile.extract(filename, path='temp')
            temp_file_path = os.path.abspath(temp_file)
            print(f'что-суем-в-функцию  {temp_file_path} ')

            full_file_path = os.path.join(self._set_new_folder(temp_file_path), jast_name)
            print(f'должно получиться -- {full_file_path} \nfile  --  {jast_name}')

            os.replace(temp_file_path, full_file_path)

        os.rmdir('temp')

        # zfile.extract(filename, path=full_file_path)

    def _set_new_folder(self, full_file_path):

        file_date = time.gmtime(os.path.getmtime(full_file_path))
        year_folder = str(file_date.tm_year)
        month_folder = str(file_date.tm_mon)

        if len(month_folder) < 2:
            month_folder = f'0{month_folder}'  # добавляем "0" перед номером месяца

        new_folder = os.path.join(self.output_folder, year_folder, month_folder)
        if not os.path.isdir(new_folder):
            os.makedirs(new_folder)

        return new_folder


obj = Sorter()
# obj.unzip_and_sort(zip_file_name='/home/nick_ka/PycharmProjects/icons_PP.zip', output_folder='icons_by_year')
obj.sort(input_folder='/home/nick_ka/PycharmProjects/icons', output_folder='icons_by_year')

# Усложненное задание (делать по желанию)
# Нужно обрабатывать zip-файл, содержащий фотографии, без предварительного извлечения файлов в папку.
# Основная функция должна брать параметром имя zip-файла и имя целевой папки.
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828

# -*- coding: utf-8 -*-

# Имеется файл events.txt вида:
#
# [2018-05-17 01:55:52.665804] NOK
# [2018-05-17 01:56:23.665804] OK
# [2018-05-17 01:56:55.665804] OK
# [2018-05-17 01:57:16.665804] NOK
# [2018-05-17 01:57:58.665804] OK
# ...


#
# Напишите программу, которая считывает файл
# и выводит число событий NOK за каждую минуту в другой файл в формате
#
# [2018-05-17 01:57] 1234
# [2018-05-17 01:58] 4321
# [2018-05-17 09:22] 2
# [2018-05-17 09:23] 2
#
# Входные параметры: файл для анализа, файл результата
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.

class LogParser:
    def __init__(self, file_name):
        self.file_name = file_name
        self.output_file = None
        self.nok_count = 0
        self.total_nok_count = 0
        self.beginning_of_the_line = ""
        self.printed_string = ''
        self.number_of_characters = 17  # 11 - за день, 14 - за час, 17 - за минуту

    def parse_in_file(self, output_file=None):
        self.output_file = output_file
        with open(self.file_name, 'r', encoding=self.what_encoding()['encoding']) as file:
            for line in file:
                bol = line[0:self.number_of_characters]
                if self.beginning_of_the_line == bol:
                    if line[-4] == 'N':
                        self.nok_count += 1
                        self.total_nok_count += 1
                else:
                    if self.nok_count > 0:
                        self.printed_string = f'{self.beginning_of_the_line}] {self.nok_count}'
                        self._print_to_file()

                    self.beginning_of_the_line = bol
                    self.nok_count = 0
                    if line[-4] == 'N':
                        self.nok_count += 1
                        self.total_nok_count += 1

            self.printed_string = f'{self.beginning_of_the_line}] {self.nok_count} \n' \
                                  f'[TOTAL COUNT OF NOK = {self.total_nok_count}]'
            self._print_to_file()

    def what_encoding(self):
        from chardet.universaldetector import UniversalDetector
        detector = UniversalDetector()
        with open(self.file_name, 'rb') as fh:
            for line in fh:
                detector.feed(line)
                if detector.done:
                    break
            detector.close()
        return detector.result

    def _print_to_file(self):

        if self.output_file:
            file = open(self.output_file, 'a', encoding='utf8')
            file.write(self.printed_string + '\n')
            file.close()
        else:
            print(self.printed_string)


obj = LogParser('events.txt')
obj.parse_in_file()

# После выполнения первого этапа нужно сделать группировку событий
#  - по часам
#  - по месяцу
#  - по году
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828

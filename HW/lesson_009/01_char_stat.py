# -*- coding: utf-8 -*-

# Подсчитать статистику по буквам в романе Война и Мир.
# Входные параметры: файл для сканирования
# Статистику считать только для букв алфавита (см функцию .isalpha() для строк)
#
# Вывести на консоль упорядоченную статистику в виде
# +---------+----------+
# |  буква  | частота  |
# +---------+----------+
# |    А    |   77777  |
# |    Б    |   55555  |
# |   ...   |   .....  |
# |    a    |   33333  |
# |    б    |   11111  |
# |   ...   |   .....  |
# +---------+----------+
# |  итого  | 9999999  |
# +---------+----------+
#
# Упорядочивание по частоте - по убыванию. Ширину таблицы подберите по своему вкусу
# Требования к коду: он должен быть готовым к расширению функциональности. Делать сразу на классах.
import zipfile
from pprint import pprint


class AnaliseText:
    def __init__(self, file_name):
        self.file_name = file_name
        self.stat = {'А': 0, 'Б': 0}

    def unzip(self):
        zfile = zipfile.ZipFile(self.file_name, 'r')
        for filename in zfile.namelist():
            zfile.extract(filename)
        self.file_name = filename

    def what_encoding(self):
        from chardet.universaldetector import UniversalDetector

        detector = UniversalDetector()
        with open(self.file_name, 'rb') as fh:
            for line in fh:
                detector.feed(line)
                if detector.done:
                    break
            detector.close()
        # print(detector.result)
        return detector.result

    def collect(self):
        if self.file_name.endswith('.zip'):
            self.unzip()

        with open(self.file_name, 'r', encoding=self.what_encoding()['encoding']) as file:
            for line in file:
                for char in line:
                    if char.isalpha():

                        if self.stat.get(char):
                            self.stat[char] += 1
                        else:
                            self.stat[char] = 1

    def _sort(self, sort_type):
        if sort_type == 'fr+':
            print("сортировка по значению")
            self.stat = dict(sorted(self.stat.items(), key=lambda x: x[1]))
        elif sort_type == 'al-':
            print("сортировка по алфавиту по убыванию")
            self.stat = dict(sorted(self.stat.items(), key=lambda x: x[0], reverse=True))
        elif sort_type == 'al+':
            print("сортировка по алфавиту по возрастанию")
            self.stat = dict(sorted(self.stat.items(), key=lambda x: x[0]))
        else:
            print('сортировка не выбрана')

    def output_data(self, output_file_name = None, sort_type = 'al+'):
        summ = 0
        self._sort(sort_type)
        if output_file_name:
            file = open(output_file_name, 'w', encoding='utf8')

            file.write('+---------+-----------+\n')
            file.write('|  буква  |  частота  |\n')
            file.write('+---------+-----------+\n')
            for elem in self.stat:
                file.write(f'|{elem: ^9}|{self.stat[elem]: ^11}|\n')
                summ += self.stat[elem]
            file.write('+---------+-----------+\n')
            file.write(f'|  итого  |{summ: ^11}|\n')
            file.write('+---------+-----------+\n')
            file.close()

        else:
            print('+---------+-----------+')
            print('|  буква  |  частота  |')
            print('+---------+-----------+')
            for elem in self.stat:
                print(f'|{elem: ^9}|{self.stat[elem]: ^11}|')
                summ += self.stat[elem]
            print('+---------+-----------+')
            print(f'|  итого  |{summ: ^11}|')
            print('+---------+-----------+')




obj = AnaliseText('voyna-i-mir.txt.zip')
# obj.what_encoding()
obj.collect()

obj.output_data(sort_type='fr+', output_file_name='new.txt')

# После выполнения первого этапа нужно сделать упорядочивание статистики
#  - по частоте по возрастанию fr+
#  - по алфавиту по возрастанию  al+
#  - по алфавиту по убыванию  al-
# Для этого пригодится шаблон проектирование "Шаблонный метод" см https://goo.gl/Vz4828

# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join

# TODO здесь ваш код
import district.central_street.house1.room1 as c_house1_r1
import district.central_street.house1.room2 as c_house1_r2
import district.central_street.house2.room1 as c_house2_r1
import district.central_street.house2.room2 as c_house2_r2
import district.soviet_street.house1.room1 as s_house1_r1
import district.soviet_street.house1.room2 as s_house1_r2
import district.soviet_street.house2.room1 as s_house2_r1
import district.soviet_street.house2.room2 as s_house2_r2

import district.soviet_street.house1 as s_house1
import district.soviet_street.house2 as s_house2

citizens = ', '.join(c_house1_r1.folks) + ', '
citizens = citizens + ', '.join(c_house1_r2.folks) + ', '
citizens = citizens + ', '.join(c_house2_r1.folks) + ', '
citizens = citizens + ', '.join(c_house2_r2.folks) + ', '
citizens = citizens + ', '.join(s_house1_r1.folks) + ', '
citizens = citizens + ', '.join(s_house1_r2.folks) + ', '
citizens = citizens + ', '.join(s_house2_r1.folks) + ', '
citizens = citizens + ', '.join(s_house2_r2.folks) + '.'

print(citizens)

# Второй вариант

citizens_list = [c_house1_r1.folks, c_house1_r2.folks, c_house2_r1.folks, c_house2_r2.folks, s_house1_r1.folks, s_house1_r2.folks,
                 s_house2_r1.folks, s_house2_r2.folks]
citizens = ''
for people in citizens_list:
    citizens = citizens + ', '.join(people) + ", "
print(citizens)

from random import randint

import simple_draw as sd

color_branch_list = [sd.COLOR_PURPLE, sd.COLOR_PURPLE,
                     sd.COLOR_DARK_ORANGE, sd.COLOR_DARK_ORANGE, sd.COLOR_DARK_ORANGE,
                     sd.COLOR_DARK_ORANGE, sd.COLOR_DARK_ORANGE, sd.COLOR_DARK_YELLOW]


def draw_branch(root_point, angle, length, width):
    if length > 2:
        vector_branch = sd.get_vector(root_point, angle, length, width)
        vector_branch.draw(color_branch_list[width])

        length = int(length * randint(65, 85)*0.01)
        if width > 1: width -= 1
        draw_branch(vector_branch.end_point, angle + 30 + randint(-5, 10), length, width)
        draw_branch(vector_branch.end_point, angle - 40 - randint(-5, 10), length, width)

    else:
        return




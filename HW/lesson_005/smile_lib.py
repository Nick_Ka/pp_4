# -*- coding: utf-8 -*-
import simple_draw as sd


def draw_smile(centr_x, crntr_y, radius, color=sd.COLOR_YELLOW, width=2):
    point = sd.get_point(centr_x, crntr_y)
    sd.circle(point, radius, color, width)
    point = sd.get_point(centr_x - (radius / 3), crntr_y + (radius / 4))
    sd.circle(point, radius / 10, color, width)
    point = sd.get_point(centr_x + (radius / 3), crntr_y + (radius / 4))
    sd.circle(point, radius / 10, color, width)
    point = sd.get_point(centr_x, crntr_y - (radius / 2))
    vector_1 = sd.get_vector(point, 150, radius / 3)
    vector_1.draw(color, width)
    vector_2 = sd.get_vector(point, 30, radius / 3)
    vector_2.draw(color, width)


def draw_person(centr_x, crntr_y, radius, color=sd.COLOR_PURPLE, width=2):
    draw_smile(centr_x, crntr_y, radius, color, width)
    point = sd.get_point(centr_x, crntr_y - radius)
    body = sd.get_vector(point, -90, radius * 3, width)
    body.draw(color)
    point = sd.get_point(centr_x, crntr_y - radius * 1.3)
    hend_l = sd.get_vector(point, -140, radius * 2, width)
    hend_r = sd.get_vector(point, -40, radius * 2, width)
    hend_l.draw(color)
    hend_r.draw(color)
    point = sd.get_point(centr_x, crntr_y - 4 * radius)
    leg_l = sd.get_vector(point, -110, radius * 2.5, width)
    leg_l.draw(color)
    leg_r = sd.get_vector(point, -70, radius * 2.5, width)
    leg_r.draw(color)


def draw_raibow(centr_x=300, crntr_y=00, radius=600, width=5):
    rainbow_colors = [sd.COLOR_RED, sd.COLOR_DARK_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN, sd.COLOR_BLUE,
                      sd.COLOR_DARK_BLUE, sd.COLOR_PURPLE]
    point = sd.get_point(centr_x, crntr_y)
    for color in rainbow_colors:
        sd.circle(point, radius, color, width)
        radius = radius + width


def draw_sun(centr_x=200, crntr_y=500, radius=50):
    point = sd.get_point(centr_x, crntr_y)
    sd.circle(point, radius, sd.COLOR_YELLOW, radius)
    for ray in range(0, 360, 30):
        sd.get_vector(point, ray, radius * 2, width=int(radius * 0.2)).draw()

def draw_snow():
    pass
    pass
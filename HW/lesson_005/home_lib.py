import simple_draw as sd


def dr_brick_2(start_point, angle=0, length=20, height=10, color=sd.COLOR_ORANGE):
    vector = sd.get_vector(start_point, angle, length)
    vector.draw(color)
    end_point = vector.end_point
    vector = sd.get_vector(end_point, 90, height)
    vector.draw(color)
    return end_point


def dr_double_row(row_point, count_bricks=15, len_br=20, hi_br=10, color=sd.COLOR_ORANGE):
    vector = sd.get_vector(row_point, 90, hi_br)
    vector.draw(color)
    for i in range(0, count_bricks, 1):
        row_point = dr_brick_2(row_point, 0, len_br, hi_br)
        if i == count_bricks - 1:
            row_point = dr_brick_2(row_point, 0, int(len_br / 2), hi_br)
            row_point = sd.get_point(row_point.x, row_point.y + hi_br)
    vector = sd.get_vector(row_point, 90, hi_br)
    vector.draw(color)

    for i in range(0, count_bricks, 1):
        row_point = dr_brick_2(row_point, 180, len_br, hi_br)
        if i == count_bricks - 1:
            row_point = dr_brick_2(row_point, 180, int(len_br / 2), hi_br)
    return sd.get_point(row_point.x, row_point.y + hi_br)


def dr_home(start_x, start_y, count_bricks_x, count_bricks_y, len_br=20, hi_br=10, color=sd.COLOR_ORANGE):
    home_point = sd.get_point(start_x, start_y)
    step = int(count_bricks_y / 3)
    width_wall = (count_bricks_x * len_br) + len_br / 2

    # рисуем нижние ряды

    for i in range(0, step, 1):
        home_point = dr_double_row(home_point, count_bricks_x, len_br=20, hi_br=10, color=color)

    fix_point = home_point

    # рисуем подоконник
    pod_point = sd.get_point(fix_point.x + (len_br * count_bricks_x / 3), fix_point.y)
    vector = sd.get_vector(pod_point, 0, (len_br * count_bricks_x / 3))
    vector.draw(color)

    # рисуем средние ряды, справа от окна
    home_point = sd.get_point((fix_point.x + (len_br * count_bricks_x * 2 / 3)), fix_point.y)

    for i in range(0, step, 1):
        home_point = dr_double_row(home_point, int(count_bricks_x / 3), len_br=20, hi_br=10, color=color)

    # рисуем средние ряды, слева от окна
    home_point = fix_point

    for i in range(0, step, 1):
        home_point = dr_double_row(home_point, int(count_bricks_x / 3), len_br=20, hi_br=10, color=color)

    # рисуем верхние ряды
    for i in range(0, step, 1):
        home_point = dr_double_row(home_point, count_bricks_x, len_br=20, hi_br=10, color=color)

    # рисуем крышу
    up_point = sd.get_point(home_point.x + width_wall / 2, home_point.y + hi_br * 6)
    sd.line(home_point, up_point, color=sd.COLOR_RED, width=3)
    right_point = sd.get_point(home_point.x + width_wall, home_point.y)
    sd.line(up_point, right_point, color=sd.COLOR_RED, width=3)
    sd.line(right_point, home_point, color=sd.COLOR_RED, width=3)


# dr_home(100, 100, 18, 18)

# sd.pause()

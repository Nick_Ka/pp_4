# -*- coding: utf-8 -*-

import argparse


class BowlingScore:
    def __init__(self):
        self.game_result = None
        self.parser = None
        self.score = 0
        self.VAL_DICT = {'Х': 20, 'X': 20, '-': 0, '/': 15, '1': 1, '2': 2, '3': 3, '4': 4,
                         '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}

    def get_score_input(self):  # пример использования c input
        self.game_result = input('введите счет: ')
        self.act()

    def get_score_argparse(self):  # пример использования argparse
        self.parser = argparse.ArgumentParser(description='Введите счет(пример  --result=X357/-3X54718-2)')
        self.parser.add_argument(
            '--result',
            type=str,
            default='',
            help='Введите счет'
        )
        self.game_result = self.parser.parse_args().result
        self.act()

    def act(self):
        if self.is_valid():
            self.score = 0
            max_len = 20
            for elem in self.game_result:
                if elem == '/':
                    self.score -= self.VAL_DICT[pred_elem]
                    if pred_elem == 'X' or pred_elem == 'Х':
                        raise ValueError('WHAT a FACK? Wrong score')
                if elem == 'X' or elem == 'Х':
                    max_len -= 1
                self.score += self.VAL_DICT[elem]
                pred_elem = elem
            if max_len < len(self.game_result):
                raise ValueError('WHAT a FACK? String TO long')
            print(f'Score = {self.score} ')
        else:
            print('некорректно введены данные')

    def is_valid(self):
        if type(self.game_result) != str:
            raise ValueError('WHAT a FACK? That NOT string')
        elif len(self.game_result) > 20:
            raise ValueError('WHAT a FACK? String TO long')
        elif len(self.game_result) < 1:
            raise ValueError('WHAT a FACK? String TO short')
        else:
            return True

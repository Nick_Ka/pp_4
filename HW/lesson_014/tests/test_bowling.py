# -*- coding: utf-8 -*-
import unittest
from unittest.mock import Mock

from HW.lesson_014.bowling import BowlingScore


class BowlingTest(unittest.TestCase):
    def setup(self):
        self.bowling = BowlingScore()

    def test_is_valid(self):
        self.setup()
        self.bowling.game_result = 'X357/-3X54718-2'
        res = self.bowling.is_valid()
        self.assertEqual(res, True)

    def test_act(self):
        self.setup()
        self.bowling.game_result = 'X357/-3X54718-2'
        self.bowling.is_valid = Mock(return_value=True)
        self.bowling.act()
        self.assertEqual(self.bowling.score, 93)



# -*- coding: utf-8 -*-


# Описание предметной области:
#
# При торгах на бирже совершаются сделки - один купил, второй продал.
# Покупают и продают ценные бумаги (акции, облигации, фьючерсы, етс). Ценные бумаги - это по сути долговые расписки.
# Ценные бумаги выпускаются партиями, от десятка до несколько миллионов штук.
# Каждая такая партия (выпуск) имеет свой торговый код на бирже - тикер - https://goo.gl/MJQ5Lq
# Все бумаги из этой партии (выпуска) одинаковы в цене, поэтому говорят о цене одной бумаги.
# У разных выпусков бумаг - разные цены, которые могут отличаться в сотни и тысячи раз.
# Каждая биржевая сделка характеризуется:
#   тикер ценнной бумаги
#   время сделки
#   цена сделки
#   обьем сделки (сколько ценных бумаг было куплено)
#
# В ходе торгов цены сделок могут со временем расти и понижаться. Величина изменения цен называтея волатильностью.
# Например, если бумага №1 торговалась с ценами 11, 11, 12, 11, 12, 11, 11, 11 - то она мало волатильна.
# А если у бумаги №2 цены сделок были: 20, 15, 23, 56, 100, 50, 3, 10 - то такая бумага имеет большую волатильность.
# Волатильность можно считать разными способами, мы будем считать сильно упрощенным способом -
# отклонение в процентах от средней цены за торговую сессию:
#   средняя цена = (максимальная цена + минимальная цена) / 2
#   волатильность = ((максимальная цена - минимальная цена) / средняя цена) * 100%
# Например для бумаги №1:
#   average_price = (12 + 11) / 2 = 11.5
#   volatility = ((12 - 11) / average_price) * 100 = 8.7%
# Для бумаги №2:
#   average_price = (100 + 3) / 2 = 51.5
#   volatility = ((100 - 3) / average_price) * 100 = 188.34%
#
# В реальности волатильность рассчитывается так: https://goo.gl/VJNmmY
#
# Задача: вычислить 3 тикера с максимальной и 3 тикера с минимальной волатильностью.
# Бумаги с нулевой волатильностью вывести отдельно.
# Результаты вывести на консоль в виде:
#   Максимальная волатильность:
#       ТИКЕР1 - ХХХ.ХХ %
#       ТИКЕР2 - ХХХ.ХХ %
#       ТИКЕР3 - ХХХ.ХХ %
#   Минимальная волатильность:
#       ТИКЕР4 - ХХХ.ХХ %
#       ТИКЕР5 - ХХХ.ХХ %
#       ТИКЕР6 - ХХХ.ХХ %
#   Нулевая волатильность:
#       ТИКЕР7, ТИКЕР8, ТИКЕР9, ТИКЕР10, ТИКЕР11, ТИКЕР12
# Волатильности указывать в порядке убывания. Тикеры с нулевой волатильностью упорядочить по имени.
#
# Подготовка исходных данных
# 1. Скачать файл https://drive.google.com/file/d/1l5sia-9c-t91iIPiGyBc1s9mQ8RgTNqb/view?usp=sharing
#       (обратите внимание на значок скачивания в правом верхнем углу,
#       см https://drive.google.com/file/d/1M6mW1jI2RdZhdSCEmlbFi5eoAXOR3u6G/view?usp=sharing)
# 2. Раззиповать средствами операционной системы содержимое архива
#       в папку python_base_source/lesson_012/trades
# 3. В каждом файле в папке trades содержится данные по сделакам по одному тикеру, разделенные запятыми.
#   Первая строка - название колонок:
#       SECID - тикер
#       TRADETIME - время сделки
#       PRICE - цена сделки
#       QUANTITY - количество бумаг в этой сделке
#   Все последующие строки в файле - данные о сделках
#
# Подсказка: нужно последовательно открывать каждый файл, вычитывать данные, высчитывать волатильность и запоминать.
# Вывод на консоль можно сделать только после обработки всех файлов.
#
# Для плавного перехода к мультипоточности, код оформить в обьектном стиле, используя следующий каркас
#
from math import inf
import os
from my_helper import time_track, what_encoding


class StockAnalyst:

    def __init__(self, file_name):
        self.file_name = file_name
        self.secid = None
        self.tradetime = 0
        self.price = 0
        self.quantyty = 0
        self.volatility = 0

    def run(self):
        with open(self.file_name, 'r', encoding=what_encoding(file_name=self.file_name)['encoding']) as file:
            max_price = 0
            min_price = inf
            for line in file:
                self.secid, self.tradetime, self.price, self.quantyty = line.split(',')
                try:
                    self.price = float(self.price)
                except ValueError:
                    continue
                if max_price < self.price:
                    max_price = self.price
                if min_price > self.price:
                    min_price = self.price
        average_price = (min_price + max_price) / 2
        self.volatility = round(((max_price - min_price) / average_price) * 100, 2)


@time_track
def main():
    vol_zero = []
    volatility_list = []

    for dirpath, dirnames, filenames in os.walk('trades'):  #trades
        for file in filenames:
            file_name = os.path.join(dirpath, file)
            obj = StockAnalyst(file_name)
            obj.run()
            if obj.volatility == 0:
                vol_zero.append(obj.secid)
            else:
                volatility_list.append((obj.volatility, obj.secid, ))

    vol_zero = sorted(vol_zero)
    volatility_list = sorted(volatility_list, reverse=True)

    print('Максимальная волатильность:')
    for i in range(0, 3, 1):
        print(f'{volatility_list[i][1]: >20} - {volatility_list[i][0]: <6}%')

    volatility_list = sorted(volatility_list, reverse=False)
    print('Минимальная волатильность:')
    for i in range(0, 3, 1):
        print(f'{volatility_list[i][1]: >20} - {volatility_list[i][0]: <6} %')

    print('Нулевая волатильность:')
    print(', '.join(vol_zero))


if __name__ == '__main__':
    main()
#
# Максимальная волатильность:
#                 SiH9 - 24.39 %
#                 PDM9 - 23.2  %
#                 PDH9 - 22.69 %
# Минимальная волатильность:
#                 CHM9 - 0.95   %
#                 GOG9 - 0.97   %
#                 RNU9 - 0.98   %
# Нулевая волатильность:
# CLM9, CYH9, EDU9, EuH0, EuZ9, JPM9, MTM9, O4H9, PDU9, PTU9, RIH0, RRG9, TRH9, VIH9
# Функция работала 32.7364 секунд(ы)




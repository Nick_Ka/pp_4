# -*- coding: utf-8 -*-

"""мой набор вспомогательных функций"""

import time


def time_track(func):
    def surrogate(*args, **kwargs):
        started_at = time.time()

        result = func(*args, **kwargs)

        ended_at = time.time()
        elapsed = round(ended_at - started_at, 4)
        print(f'Функция работала {elapsed} секунд(ы)')
        return result
    return surrogate


def what_encoding(file_name):
    from chardet.universaldetector import UniversalDetector
    detector = UniversalDetector()
    with open(file_name, 'rb') as file:
        for line in file:
            detector.feed(line)
            if detector.done:
                print(f'detector = {detector.result}')
                break
        detector.close()
    return detector.result

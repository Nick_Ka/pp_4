# -*- coding: utf-8 -*-


# Задача: вычислить 3 тикера с максимальной и 3 тикера с минимальной волатильностью в МНОГОПОТОЧНОМ стиле
#
# Бумаги с нулевой волатильностью вывести отдельно.
# Результаты вывести на консоль в виде:
#   Максимальная волатильность:
#       ТИКЕР1 - ХХХ.ХХ %
#       ТИКЕР2 - ХХХ.ХХ %
#       ТИКЕР3 - ХХХ.ХХ %
#   Минимальная волатильность:
#       ТИКЕР4 - ХХХ.ХХ %
#       ТИКЕР5 - ХХХ.ХХ %
#       ТИКЕР6 - ХХХ.ХХ %
#   Нулевая волатильность:
#       ТИКЕР7, ТИКЕР8, ТИКЕР9, ТИКЕР10, ТИКЕР11, ТИКЕР12
# Волатильности указывать в порядке убывания. Тикеры с нулевой волатильностью упорядочить по имени.
#

import os
import threading
from my_helper import time_track, what_encoding
from math import inf


class StockMarket(threading.Thread):
    def __init__(self, folder):
        super().__init__()
        self.folder = folder
        self.ticker_list = []
        self.vol_zero = []
        self.vol_up = []
        self.vol_down = []
        self.top_three = [['', 0], ['', 0], ['', 0], ]
        self.bottom_three = [['', inf], ['', inf], ['', inf], ]

    def run(self):
        for dirpath, dirnames, filenames in os.walk(self.folder):  # trades
            for file in filenames:
                file_name = os.path.join(dirpath, file)
                self.ticker_list.append(StockAnalyst(file_name))
            for ticker in self.ticker_list:
                ticker.start()
            for ticker in self.ticker_list:
                ticker.join()

            for ticker in self.ticker_list:
                if ticker.volatility == 0:
                    self.vol_zero.append(ticker.secid)
                else:
                    tick_dict = [ticker.secid, ticker.volatility]
                    self.vol_up = self.triple_sorting_up(ticker=tick_dict)
                    self.vol_down = self.triple_sorting_down(ticker=tick_dict)

        self.vol_zero = sorted(self.vol_zero)
        print('Максимальная волатильность:')
        for i in range(0, 3, 1):
            print(f'{self.vol_up[i][0]: >20} - {self.vol_up[i][1]: <6} %')
        print('Минимальная волатильность:')
        for i in range(0, 3, 1):
            print(f'{self.vol_down[i][0]: >20} - {self.vol_down[i][1]: <6} %')
        print('Нулевая волатильность:')
        print(', '.join(self.vol_zero))

    def triple_sorting_up(self, ticker):
        if ticker[1] > self.top_three[0][1]:
            self.top_three[2] = self.top_three[1]
            self.top_three[1] = self.top_three[0]
            self.top_three[0] = ticker
        elif ticker[1] > self.top_three[1][1]:
            self.top_three[2] = self.top_three[1]
            self.top_three[1] = ticker
        elif ticker[1] > self.top_three[2][1]:
            self.top_three[2] = ticker
        return self.top_three

    def triple_sorting_down(self, ticker):
        if ticker[1] < self.bottom_three[0][1]:
            self.bottom_three[2] = self.bottom_three[1]
            self.bottom_three[1] = self.bottom_three[0]
            self.bottom_three[0] = ticker
        elif ticker[1] < self.bottom_three[1][1]:
            self.bottom_three[2] = self.bottom_three[1]
            self.bottom_three[1] = ticker
        elif ticker[1] < self.bottom_three[2][1]:
            self.bottom_three[2] = ticker
        return self.bottom_three


class StockAnalyst(threading.Thread):

    def __init__(self, file_name):
        super().__init__()
        self.file_name = file_name
        self.secid = None
        self.tradetime = 0
        self.price = 0
        self.quantyty = 0
        self.volatility = 0

    def run(self):
        with open(self.file_name, 'r', encoding=what_encoding(file_name=self.file_name)['encoding']) as file:
            max_price = 0
            min_price = inf
            for line in file:
                self.secid, self.tradetime, self.price, self.quantyty = line.split(',')
                try:
                    self.price = float(self.price)
                except ValueError:
                    continue
                if max_price < self.price:
                    max_price = self.price
                if min_price > self.price:
                    min_price = self.price
        average_price = (min_price + max_price) / 2
        self.volatility = round(((max_price - min_price) / average_price) * 100, 2)


@time_track
def main():

    sm = StockMarket('trades')
    sm.start()
    sm.join()


if __name__ == '__main__':
    main()
#
# Максимальная волатильность:
#                 SiH9 - 24.39 %
#                 PDM9 - 23.2  %
#                 PDH9 - 22.69 %
# Минимальная волатильность:
#                 CHM9 - 0.95   %
#                 GOG9 - 0.97   %
#                 RNU9 - 0.98   %
# Нулевая волатильность:
# CLM9, CYH9, EDU9, EuH0, EuZ9, JPM9, MTM9, O4H9, PDU9, PTU9, RIH0, RRG9, TRH9, VIH9
# Функция работала 23.934 секунд(ы)
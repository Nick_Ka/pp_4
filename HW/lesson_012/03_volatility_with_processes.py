# -*- coding: utf-8 -*-


# Задача: вычислить 3 тикера с максимальной и 3 тикера с минимальной волатильностью в МНОГОПРОЦЕССНОМ стиле
#
# Бумаги с нулевой волатильностью вывести отдельно.
# Результаты вывести на консоль в виде:
#   Максимальная волатильность:
#       ТИКЕР1 - ХХХ.ХХ %
#       ТИКЕР2 - ХХХ.ХХ %
#       ТИКЕР3 - ХХХ.ХХ %
#   Минимальная волатильность:
#       ТИКЕР4 - ХХХ.ХХ %
#       ТИКЕР5 - ХХХ.ХХ %
#       ТИКЕР6 - ХХХ.ХХ %
#   Нулевая волатильность:
#       ТИКЕР7, ТИКЕР8, ТИКЕР9, ТИКЕР10, ТИКЕР11, ТИКЕР12
# Волатильности указывать в порядке убывания. Тикеры с нулевой волатильностью упорядочить по имени.
#
import os
from multiprocessing import Process, Pipe
from my_helper import time_track, what_encoding
from math import inf



class StockAnalyst(Process):

    def __init__(self, file_name, conn):
        super(StockAnalyst, self).__init__()
        self.file_name = file_name
        self.volatility = 0
        self.price = 0
        self.secid = None
        self.conn = conn

    def run(self):
        with open(self.file_name, 'r', encoding=what_encoding(file_name=self.file_name)['encoding']) as file:
            max_price = 0
            min_price = inf
            for line in file:
                self.secid, _, self.price, _ = line.split(',')
                try:
                    self.price = float(self.price)
                except ValueError:
                    continue
                if max_price < self.price:
                    max_price = self.price
                if min_price > self.price:
                    min_price = self.price
        average_price = (min_price + max_price) / 2
        self.volatility = round(((max_price - min_price) / average_price) * 100, 2)

        self.conn.send([self.secid, self.volatility])
        self.conn.close()


@time_track
def main():

    volatility_zero, volatility_list, stock_analysts, pipes = [], [], [], []

    for dirpath, dirnames, filenames in os.walk('trades'):  #trades
        for file in filenames:
            file_name = os.path.join(dirpath, file)
            parent_conn, child_conn = Pipe()
            stock_an = StockAnalyst(file_name, conn=child_conn)
            stock_analysts.append(stock_an)
            pipes.append(parent_conn)

    for sa in stock_analysts:
        sa.start()

    for conn in pipes:
        secid, volatility = conn.recv()
        if volatility == 0:
            volatility_zero.append(secid)
        else:
            volatility_list.append([volatility, secid])

    for sa in stock_analysts:
        sa.join()

    volatility_zero = sorted(volatility_zero)
    volatility_list = sorted(volatility_list, reverse=True)

    print('Максимальная волатильность:')
    for i in range(0, 3, 1):
        print(f'{volatility_list[i][1]: >20} - {volatility_list[i][0]: <6}%')

    volatility_list = sorted(volatility_list, reverse=False)
    print('Минимальная волатильность:')
    for i in range(0, 3, 1):
        print(f'{volatility_list[i][1]: >20} - {volatility_list[i][0]: <6} %')

    print('Нулевая волатильность:')
    print(', '.join(volatility_zero))


if __name__ == '__main__':
    main()
#
# Максимальная волатильность:
#                 SiH9 - 24.39 %
#                 PDM9 - 23.2  %
#                 PDH9 - 22.69 %
# Минимальная волатильность:
#                 CHM9 - 0.95   %
#                 GOG9 - 0.97   %
#                 RNU9 - 0.98   %
# Нулевая волатильность:
# CLM9, CYH9, EDU9, EuH0, EuZ9, JPM9, MTM9, O4H9, PDU9, PTU9, RIH0, RRG9, TRH9, VIH9
# Функция работала 22.5176 секунд(ы)

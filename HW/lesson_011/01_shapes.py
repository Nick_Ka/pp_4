# -*- coding: utf-8 -*-

import simple_draw as sd


# На основе вашего кода из решения lesson_004/01_shapes.py сделать функцию-фабрику,
# которая возвращает функции рисования треугольника, четырехугольника, пятиугольника и т.д.
#
# Функция рисования должна принимать параметры
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Функция-фабрика должна принимать параметр n - количество сторон.


def get_polygon(n):
    def draw_polygon(start_point, angle, length, width):
        point = start_point

        for _ in range(n):
            vector = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
            vector.draw(color='red')
            angle += (180 - (180 * (n - 2)) / n)
            point = vector.end_point

    return draw_polygon


sd.start_drawing()

draw_triangle = get_polygon(n=3)
draw_pentagon = get_polygon(n=5)
draw_octagon = get_polygon(n=8)

draw_triangle(start_point=sd.get_point(100, 100), angle=0, length=100, width=3)
draw_pentagon(start_point=sd.get_point(300, 300), angle=0, length=70, width=2)
draw_octagon(start_point=sd.get_point(400, 400), angle=0, length=80, width=3)

sd.finish_drawing()
sd.pause()

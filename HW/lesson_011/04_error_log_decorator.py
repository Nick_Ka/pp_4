# -*- coding: utf-8 -*-

# Написать декоратор, который будет логировать (записывать в лог файл)
# ошибки из декорируемой функции и выбрасывать их дальше.
#
# Имя файла лога - function_errors.log
# Формат лога: <имя функции> <параметры вызова> <тип ошибки> <текст ошибки>
# Лог файл открывать каждый раз при ошибке в режиме 'a'

import datetime


def log_errors(file_name):

    def the_real_decorator(func):

        def surrogate(*args, **kwargs):
            result = False
            try:
                result = func(*args, **kwargs)
            except Exception as exc:
                with open(file_name, 'a', encoding='utf8') as file:
                    now = datetime.datetime.now()
                    file.write(f'{now} - Вызвана функция - {func.__name__}\nInvalid format: {exc}\n')
            return result
        return surrogate

    return the_real_decorator


# Проверить работу на следующих функциях
@log_errors(file_name='function_errors.log')
def perky(param):
    return param / 0


@log_errors(file_name='function_errors.log')
def check_line(line):
    name, email, age = line.split(' ')
    if not name.isalpha():
        raise ValueError("it's not a name")
    elif '@' not in email or '.' not in email:
        raise ValueError("it's not a email")
    elif not 10 <= int(age) <= 99:
        raise ValueError('Age not in 10..99 range')
    else:
        print(line)



lines = [
    'Ярослав bxh@ya.ru 600',
    'Земфира tslzp@mail.ru 52',
    'Тролль nsocnzas.mail.ru 82',
    'Джигурда wqxq@gmail.com 29',
    'Земфира 86',
    'Равшан wmsuuzsxi@mail.ru 35',
]
for line in lines:
    try:
        check_line(line)
    except Exception as exc:
        print(f'Invalid format: {exc}//////////////////')
perky(param=42)


# Усложненное задание (делать по желанию).
# Написать декоратор с параметром - именем файла
#
# @log_errors('function_errors.log')
# def func():
#     pass
###########  для этого в декоратор передаем параметр, а в декораторе добавляем вложенную функцию с параметром 'func'
###########

# def внешний_декоратор(file_name):
#
#     def the_real_decorator(func):
#
#         def surrogate(*args, **kwargs):
#             result = func(args + kwargs + file_name) # некое действие со всеми параметрами
#             return result   #  возвращаем обернутую функцию
#         return surrogate    # выходим из сурогата
#
#     return the_real_decorator   #   выходим из декорптора

#


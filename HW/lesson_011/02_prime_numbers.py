# -*- coding: utf-8 -*-


# Есть функция генерации списка простых чисел


def get_prime_numbers(n):
    prime_numbers = []
    for number in range(2, n + 1):
        for prime in prime_numbers:
            if number % prime == 0:
                break
        else:
            prime_numbers.append(number)
    return prime_numbers


# Часть 1
# На основе алгоритма get_prime_numbers создать класс итерируемых обьектов,
# который выдает последовательность простых чисел до n
#
# Распечатать все простые числа до 10000 в столбик


class PrimeNumbers:

    def __init__(self, n):
        self.n = n

    def __iter__(self):
        # обнуляем счетчик перед циклом
        self.prime_numbers = []
        # возвращаем ссылку на себя - я буду итератором!
        return self

    def __next__(self):
        # а этот метод возвращает значения по требованию python
        for num in range(2, self.n + 1):
            for prime in self.prime_numbers:
                if num % prime == 0:
                    break
            else:
                self.prime_numbers.append(num)
                return num
        raise StopIteration()  # признак того, что больше возвращать нечего


# prime_number_iterator = PrimeNumbers(n=100)
# for number in prime_number_iterator:
#     print(number)


# TODO после подтверждения части 1 преподователем, можно делать
# Часть 2
# Теперь нужно создать генератор, который выдает последовательность простых чисел до n
# Распечатать все простые числа до 10000 в столбик


def prime_numbers_generator(n):
    prime_numbers = []
    for num in range(2, n + 1):
        for prime in prime_numbers:
            if num % prime == 0:
                break
        else:
            prime_numbers.append(num)
            yield num


def lucky_num(num):
    if num < 11: return False
    num_str = str(num)
    half_line = len(num_str) // 2
    l_summ = 0
    for i in range(0, half_line, 1):
        l_summ += int(num_str[i])
    r_summ = 0
    for i in range(1, half_line + 1, 1):
        r_summ += int(num_str[-i])
    if l_summ == r_summ:
        return num
    else:
        return False


def palindrom_num(num):
    if num < 11:
        return False
    num_str = str(num)
    half_line = len(num_str) // 2
    for i in range(0, half_line, 1):
        if num_str[i] != num_str[-(i + 1)]:
            return False
    return num


# for number in prime_numbers_generator(n=100):
# print(list(filter(lucky_num, prime_numbers_generator(n=10000))))

for n in filter(palindrom_num, prime_numbers_generator(n=10000)):
    print(n)

# Часть 3
# Написать несколько функций-фильтров, которые выдает True, если число:
# 1) "счастливое" в обыденном пониманиии - сумма первых цифр равна сумме последних
#       Если число имеет нечетное число цифр (например 727 или 92083),
#       то для вычисления "счастливости" брать равное количество цифр с начала и конца:
#           727 -> 7(2)7 -> 7 == 7 -> True
#           92083 -> 92(0)83 -> 9+2 == 8+3 -> True
# 2) "палиндромное" - одинаково читающееся в обоих направлениях. Например 723327 и 101
# 3) придумать свою (https://clck.ru/GB5Fc в помощь)
#
# Подумать, как можно применить функции-фильтры к полученной последовательности простых чисел
# для получения, к примеру: простых счастливых чисел, простых палиндромных чисел,
# простых счастливых палиндромных чисел и так далее. Придумать не менее 2х способов.
#
# Подсказка: возможно, нужно будет добавить параметр в итератор/генератор.

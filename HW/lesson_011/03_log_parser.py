# -*- coding: utf-8 -*-

# На основе своего кода из lesson_009/02_log_parser.py напишите итератор (или генератор)
# котрый читает исходный файл events.txt и выдает число событий NOK за каждую минуту
# <время> <число повторений>
#
# пример использования:
#
# grouped_events = <создание итератора/генератора>
# for group_time, event_count in grouped_events:
#     print(f'[{group_time}] {event_count}')
#
# на консоли должно появится что-то вроде
#
# [2018-05-17 01:57] 1234

class Events:
    def __init__(self, file_name):
        self.file_name = file_name
        self.beginning_of_the_line = ""
        self.nok_count = 0

    def __iter__(self):
        self.file = open(self.file_name, 'r', encoding=self.what_encoding()['encoding'])
        return self

    def __next__(self):
        res = ['', '']
        for line in self.file:
            b_o_l = line[0:17]

            if self.beginning_of_the_line == b_o_l:
                if line[-4] == 'N':
                    self.nok_count += 1
            else:
                if self.nok_count > 0:
                    res = [self.beginning_of_the_line, self.nok_count]
                self.beginning_of_the_line = b_o_l
                self.nok_count = 0
                if line[-4] == 'N':
                    self.nok_count += 1
            if res != ['', '']:
                return res
        self.file.close()
        raise StopIteration()  # признак того, что больше возвращать нечего
    # последний NOK не парсится #############################################


    def what_encoding(self):
        from chardet.universaldetector import UniversalDetector
        detector = UniversalDetector()
        with open(self.file_name, 'rb') as file:
            for line in file:
                detector.feed(line)
                if detector.done:
                    print(f'detector = {detector.result}')
                    break
            detector.close()
        return detector.result


grouped_events = Events(file_name='events.txt')

for group_time, event_count in grouped_events:
    print(f'{group_time}] {event_count}')


# -*- coding: utf-8 -*-


import os
from tkinter import *
from PIL import Image, ImageDraw, ImageFont, ImageColor


# Заполнить все поля в билете на самолет.
# Создать функцию, принимающую параметры: ФИО, откуда, куда, дата вылета,
# и заполняющую ими шаблон билета Skillbox Airline.
# Шаблон взять в файле lesson_013/images/ticket_template.png
# Пример заполнения lesson_013/images/ticket_sample.png
# Подходящий шрифт искать на сайте ofont.ru


class MakeTicket:

    def __init__(self):
        self.lbl_bottom = None
        self.txt_fio = None
        self.txt_from = None
        self.txt_to = None
        self.txt_date = None

        self.fio = ''
        self.from_ = ''
        self.to = ''
        self.date = ''
        self.im = None
        self.template = 'images/ticket_template.png'
        self.font_path = os.path.join("fonts", "ofont.ru_Chadwick Cyrillic.ttf")

    def input_data(self):
        self.fio = input('Введите ФИО:             ')
        self.from_ = input('Введите место вылета:    ')
        self.to = input('Введите место прилета:   ')
        self.date = input('Введите дату:             ')

    def input_data_console(self):
        import argparse

        parser = argparse.ArgumentParser(description="Заполнение билета")
        print("Введите данные для заполнения билета:\n"
              "-n 'ФИО'\n"
              "-f 'место вылета'\n"
              "-t 'место прилета'\n"
              "-d 'дату вылета'\n")

        parser.add_argument('-n', dest="_fio", type=str,
                            required=True, help='Введены некорректные данные')
        parser.add_argument('-f', dest="_from", type=str,
                            required=True, help='Введены некорректные данные')
        parser.add_argument('-t', dest="_to", type=str,
                            required=True, help='Введены некорректные данные')
        parser.add_argument('-d', dest="_date", type=str,
                            required=True, help='Введены некорректные данные')

        args = parser.parse_args()
        self.fio = args._fio
        self.from_ = args._from
        self.to = args._to
        self.date = args._date

    def _clicked(self):
        self.fio = self.txt_fio.get()
        self.from_ = self.txt_from.get()
        self.to = self.txt_to.get()
        self.date = self.txt_date.get()

        self.lbl_bottom.configure(text="Билет заполонен...")

    def input_data_form(self):
        window = Tk()
        window.geometry('670x400')
        window.title("Заполнение билета")
        lbl_fio = Label(window, text='Введите ФИО:               ', font=("Arial Bold", 12))
        lbl_fio.grid(column=0, row=0, padx=10, pady=20)
        self.txt_fio = Entry(window, width=50)
        self.txt_fio.grid(column=1, row=0)
        self.txt_fio.focus()
        lbl_from = Label(window, text='Введите место вылета:    ', font=("Arial Bold", 12))
        lbl_from.grid(column=0, row=1, pady=20)
        self.txt_from = Entry(window, width=50)
        self.txt_from.grid(column=1, row=1, pady=20)
        lbl_to = Label(window, text='Введите место прилета:   ', font=("Arial Bold", 12))
        lbl_to.grid(column=0, row=2, pady=20)
        self.txt_to = Entry(window, width=50)
        self.txt_to.grid(column=1, row=2)
        lbl_date = Label(window, text="Введите дату:               ", font=("Arial Bold", 12))
        lbl_date.grid(column=0, row=3, pady=20)
        self.txt_date = Entry(window, width=50)
        self.txt_date.grid(column=1, row=3)

        self.lbl_bottom = Label(window, text="", font=("Arial Bold", 12))
        self.lbl_bottom.grid(column=0, row=5, pady=100)
        btn = Button(window, text="Отправить", command=self._clicked)
        btn.grid(column=1, row=5)

        window.mainloop()

    def make(self):
        with Image.open(self.template) as im:
            self.im = im
            self._type_text(text=self.fio, x=46, y=125)
            self._type_text(text=self.from_, x=46, y=192)
            self._type_text(text=self.to, x=46, y=257)
            self._type_text(text=self.date, x=287, y=257)
            self.im.save('probe.png')  # (672, 401)
            self.im.show()
            print('Post card saved')

    def _type_text(self, text, x, y):
        draw = ImageDraw.Draw(self.im)
        font = ImageFont.truetype(self.font_path, size=15)
        draw.text((x, y), text, font=font, fill=ImageColor.colormap['black'])


if __name__ == '__main__':
    ticket = MakeTicket()  # fio='NICK KA', from_='MOSCOW', to='SPB', date='01/05')
    # ticket.input_data()
    # ticket.input_data_console()
    ticket.input_data_form()
    ticket.make()

# Усложненное задание (делать по желанию).
# Написать консольный скрипт c помощью встроенного python-модуля agrparse.
# Скрипт должен принимать параметры:
#   --fio - обязательный, фамилия.
#   --from - обязательный, откуда летим.
#   --to - обязательный, куда летим.
#   --date - обязательный, когда летим.
#   --save_to - необязательный, путь для сохранения заполненнего билета.
# и заполнять билет.

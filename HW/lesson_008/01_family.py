# -*- coding: utf-8 -*-

from termcolor import cprint
from random import randint


######################################################## Часть первая
#
# Создать модель жизни небольшой семьи.
#
# Каждый день участники жизни могут делать только одно действие.
# Все вместе они должны прожить год и не умереть.
#
# Муж может:
#   есть,
#   играть в WoT,
#   ходить на работу,
# Жена может:
#   есть,
#   покупать продукты,
#   покупать шубу,
#   убираться в доме,

# Все они живут в одном доме, дом характеризуется:
#   кол-во денег в тумбочке (в начале - 100)
#   кол-во еды в холодильнике (в начале - 50)
#   кол-во грязи (в начале - 0)
#
# У людей есть имя, степень сытости (в начале - 30) и степень счастья (в начале - 100).
#
# Любое действие, кроме "есть", приводит к уменьшению степени сытости на 10 пунктов
# Кушают взрослые максимум по 30 единиц еды, степень сытости растет на 1 пункт за 1 пункт еды.
# Степень сытости не должна падать ниже 0, иначе чел умрет от голода.
#
# Деньги в тумбочку добавляет муж, после работы - 150 единиц за раз.
# Еда стоит 10 денег 10 единиц еды. Шуба стоит 350 единиц.
#
# Грязь добавляется каждый день по 5 пунктов, за одну уборку жена может убирать до 100 единиц грязи.
# Если в доме грязи больше 90 - у людей падает степень счастья каждый день на 10 пунктов,
# Степень счастья растет: у мужа от игры в WoT (на 20), у жены от покупки шубы (на 60, но шуба дорогая)
# Степень счастья не должна падать ниже 10, иначе чел умрает от депресии.
#
# Подвести итоги жизни за год: сколько было заработано денег, сколько сьедено еды, сколько куплено шуб.


class House:

    def __init__(self):
        self.money = 100
        self.food = 100
        self.cats_food = 30
        self.mud = 0
        self.cats_count = 0

    def __str__(self):
        return 'денег - {}, еды - {}, кошачей еды - {}, грязи - {}'.format(self.money,
                                                                           self.food, self.cats_food, self.mud)


class Man:
    fur_count = 0
    rest_days = 0
    working_days = 0

    def __init__(self, name, home):
        self.fullness = 30
        self.happiness = 100
        self.name = name
        self.home = home

    def __str__(self):
        return '{}, сытость - {}, счастья - {}'.format(self.name, self.fullness, self.happiness)

    def eat(self):
        self.fullness += 30
        self.home.food -= 30

    def spend_a_time(self):
        self.happiness += 20
        self.fullness -= 1
        Man.rest_days += 1

    def pet_the_cat(self):
        self.happiness += 5
        cprint('{}, гладит кота, счастья - {}'.format(self.name, self.happiness), color='green')

    # def take_the_cat(self):


class Husband(Man):
    def act(self):
        if self.fullness <= 10:
            self.eat()
        elif self.home.money <= 50:
            self.work()
        elif self.happiness <= 10:
            self.gaming()
        # elif self.fullness <= 0:
        #     cprint('{} умер от голода :( '.format(self.name), color='red')
        # elif self.happiness <= 10:
        #     cprint('{} умер от дипрессии :( '.format(self.name), color='red')
        else:
            dice = randint(1, 6)
            if dice == 1:
                self.work()
            elif dice == 2:
                self.eat()
            elif dice == 3:
                if self.home.cats_count >= 1:
                    self.pet_the_cat()
                else:
                    self.gaming()
            else:
                self.gaming()

    def eat(self):
        super().eat()
        cprint('{} - поел'.format(self.name), color='green')

    def work(self):
        Man.working_days += 1
        self.home.money += 150
        self.fullness -= 10
        cprint('{} - заработал бабла, теперь дунег в тумбочке стало - {}'.format(self.name,
                                                                                 self.home.money), color='green')

    def gaming(self):
        super().spend_a_time()
        cprint('{} - играл весь день, теперь счастья полные штаны - {}'.format(self.name,
                                                                               self.happiness), color='green')


class Wife(Man):
    def act(self):
        if self.fullness <= 10:
            self.eat()
        elif self.home.food <= 30:
            self.shopping()
        elif self.home.cats_food < 20:
            self.pet_shopping()
        elif self.happiness <= 20:
            self.buy_fur_coat()
        elif self.home.mud >= 60:
            self.clean_house()
        # elif self.fullness <= 0:
        #     cprint('{} умерла от голода :( '.format(self.name), color='red')
        # elif self.happiness <= 10:
        #     cprint('{} умерла от дипрессии :( '.format(self.name), color='red')
        else:
            dice = randint(1, 6)
            if dice == 1:
                self.clean_house()
            elif dice == 2:
                self.shopping()
            elif dice == 3:
                self.eat()
            elif dice == 4:
                if self.home.cats_count >= 1:
                    self.pet_the_cat()
                else:
                    self.watched_series()
            elif dice > 4:
                self.watched_series()

    def eat(self):
        super().eat()
        cprint('{} - поела'.format(self.name), color='green')

    def shopping(self):
        if self.home.food <= 500:
            self.home.money -= 100
            self.home.food += 100
            self.fullness -= 10
            cprint('{} - потратила бабла, теперь еды в холодильнике стало - {}'.format(self.name,
                                                                                       self.home.food), color='green')
        elif self.home.money >= 1000:
            cprint('{} - купила туфельки :)'.format(self.name, color='green'))
            self.home.money -= 100
            self.happiness += 30
            self.fullness -= 10
        else:
            cprint('{} - хватит тратить бабло, мы экономим :('.format(self.name), color='green')
            self.watched_series()

    def pet_shopping(self):
        if self.home.cats_count >= 1 and self.home.cats_food <= 30:
            self.home.cats_food += 30 * self.home.cats_count
            self.home.money -= 15 * self.home.cats_count
            cprint('{} - порадовала кота, теперь еды для кота стало - {}'.format(self.name,
                                                                                 self.home.cats_food), color='green')

    def buy_fur_coat(self):
        self.happiness += 60
        self.fullness -= 10
        Man.fur_count += 1
        cprint('{} - купила шубу, теперь счастья полные штаны - {}'.format(self.name,
                                                                           self.happiness), color='green')

    def clean_house(self):
        self.happiness -= 10
        self.fullness -= 10
        if self.home.mud <= 100:
            self.home.mud = 0
        else:
            self.home.mud -= 100
        cprint('{} - убралась в доме, теперь в доме стало чисто'.format(self.name), color='green')

    def watched_series(self):
        super().spend_a_time()
        cprint('{} - смотрела сериал, и счастлива на - {}'.format(self.name, self.happiness), color='green')


class Cat:
    dead_cat = 0
    dead_days = []

    def __init__(self, name, home):
        self.home = home
        self.name = name
        self.fullness = 30
        self.home.cats_count += 1

    def __str__(self):
        return '{} , сытость - {}'.format(self.name, self.fullness)

    def act(self):
        if self.name is None:
            return
        elif self.fullness <= 0:
            Cat.dead_cat += 1
            cprint('{} помер от голода, печалька....'.format(self.name), color='red')

            self.name = None
            if self.home.cats_count > 1:
                self.home.cats_count -= 1
            else:
                self.home.cats_count = 0
        elif self.fullness <= 10:
            self.eat()
        else:
            dice = randint(1, 6)
            if dice == 1:
                self.eat()
            elif dice == 2:
                self.sleep()
            else:
                self.soil()

    def eat(self):
        if self.home.cats_food >= 10:
            self.fullness += 20
            self.home.cats_food -= 10
            cprint('{} поел'.format(self.name), color='green')
        else:
            cprint('{} орёт МЯУУУУУУ!!!!!!!!!!'.format(self.name), color='red')
            self.soil()
            self.fullness += self.home.cats_food * 2
            self.home.cats_food = 0

    def sleep(self):
        self.fullness -= 10
        cprint('{} поспал'.format(self.name), color='green')

    def soil(self):
        self.fullness -= 10
        self.home.mud += 5
        cprint('{} драл обои, и грязи стало - {}'.format(self.name, self.home.mud), color='green')


# home = House()
# serge = Husband(name='Сережа', home=home)
# masha = Wife(name='Маша', home=home)
# murzik = Cat(name='Мурзик', home=home)
# barsik = Cat(name='Барсик', home=home)
#
# for day in range(365):
#     cprint('================== День {} =================='.format(day), color='red')
#     home.mud += 5
#     serge.act()
#     masha.act()
#     murzik.act()
#     barsik.act()
#     cprint(serge, color='cyan')
#     cprint(masha, color='cyan')
#     cprint(murzik, color='cyan')
#     cprint(barsik, color='cyan')
#     cprint(home, color='cyan')


######################################################## Часть вторая
#
# После подтверждения учителем первой части надо
# отщепить ветку develop и в ней начать добавлять котов в модель семьи
#
# Кот может:
#   есть,
#   спать,
#   драть обои
#
# Люди могут:
#   гладить кота (растет степень счастья на 5 пунктов)
#
# В доме добавляется:
#   еда для кота (в начале - 30)
#
# У кота есть имя и степень сытости (в начале - 30)
# Любое действие кота, кроме "есть", приводит к уменьшению степени сытости на 10 пунктов
# Еда для кота покупается за деньги: за 10 денег 10 еды.
# Кушает кот максимум по 10 единиц еды, степень сытости растет на 2 пункта за 1 пункт еды.
# Степень сытости не должна падать ниже 0, иначе кот умрет от голода.
#
# Если кот дерет обои, то грязи становится больше на 5 пунктов


######################################################## Часть вторая бис
#
# После реализации первой части надо в ветке мастер продолжить работу над семьей - добавить ребенка
#
# Ребенок может:
#   есть,
#   спать,
#
# отличия от взрослых - кушает максимум 10 единиц еды,
# степень счастья  - не меняется, всегда ==100 ;)

class Child(Man):
    def act(self):
        if self.fullness <= 10:
            self.eat()
        else:
            self.sleep()

    def eat(self):
        self.fullness += 10
        cprint('{} поел, сытость - {}'.format(self.name, self.fullness), color='yellow')

    def sleep(self):
        self.fullness -= 10
        cprint('{} поспал, сытость - {}'.format(self.name, self.fullness), color='yellow')


######################################################## Часть третья
#
# после подтверждения учителем второй части (обоих веток)
# влить в мастер все коммиты из ветки develop и разрешить все конфликты
# отправить на проверку учителем.

cats_list = []
cats_names = ['Мурзик', "Барсик", "Васька", "Бонька", "Леопольд", "Фу_Бля"]
home = House()
serge = Husband(name='Сережа', home=home)
masha = Wife(name='Маша', home=home)
kolya = Child(name='Коля', home=home)

for day in range(365):
    cprint('================== День {} =================='.format(day), color='red')
    home.mud += 5
    serge.act()
    masha.act()
    kolya.act()

    cprint(serge, color='cyan')
    cprint(masha, color='cyan')
    cprint(kolya, color='cyan')
    cprint(home, color='magenta')

    if home.money >= 1300:
        new_cat = Cat(name=cats_names[randint(0, len(cats_names) - 1)], home=home)
        cats_list.append(new_cat)
        cprint('КУПИЛИ КОТА!!!! Назвали {}'.format(new_cat.name), color='red')
        home.money -= 1000
    if cats_list:
        for cat in cats_list:
            cat.act()
            cprint(cat, color='cyan')
            if cat.name is None:
                cats_list.remove(cat)
                Cat.dead_days.append(day)
                break

print('завели {} котов'.format(home.cats_count))
print('заморили голодом {} котов  --  '.format(Cat.dead_cat), Cat.dead_days)

print('купили {} шуб'.format(Man.fur_count))
print('пинали балду {} дней'.format(Man.rest_days))
print('вкалывали {} дней'.format(Man.working_days))

# Усложненное задание (делать по желанию)
#
# Сделать из семьи любителей котов - пусть котов будет 3, или даже 5-10.
# Коты должны выжить вместе с семьей!
#
# Определить максимальное число котов, которое может прокормить эта семья при значениях зарплаты от 50 до 400.
# Для сглаживание случайностей моделирование за год делать 3 раза, если 2 из 3х выжили - считаем что выжили.
#
# Дополнительно вносить некий хаос в жизнь семьи
# - N раз в год вдруг пропадает половина еды из холодильника (коты?)
# - K раз в год пропадает половина денег из тумбочки (муж? жена? коты?!?!)
# Промоделировать - как часто могут случаться фейлы что бы это не повлияло на жизнь героев?
#   (N от 1 до 5, K от 1 до 5 - нужно вычислит максимумы N и K при котором семья гарантированно выживает)
#
# в итоге должен получится приблизительно такой код экспериментов
# for food_incidents in range(6):
#   for money_incidents in range(6):
#       life = Simulation(money_incidents, food_incidents)
#       for salary in range(50, 401, 50):
#           max_cats = life.experiment(salary)
#           print(f'При зарплате {salary} максимально можно прокормить {max_cats} котов')

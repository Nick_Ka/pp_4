
from decimal import Decimal
import logging, json, re
from termcolor import cprint

my_json = {
  "Location_0_tm0": [
    "Mob_exp10_tm0",
    {
      "Location_1_tm10400000": [
        ["Mob_exp20_tm200",
        "Mob_exp20_tm200"],
        {
          "Location_3_tm330000000": [
            {
              "Location_7_tm333000000": [
                {
                  "Location_10_tm551000000": [
                    [
                      "Mob_exp25_tm1",
                      "Mob_exp30_tm1",
                      "Mob_exp20_tm1",
                      "Mob_exp24_tm1"
                    ],
                    {
                      "Location_12_tm0.0987654320": [
                        "Boss100_exp100_tm10",
                        "Boss200_exp30_tm10"
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "Location_2_tm333000000": [
        "Mob_exp20_tm167710",
        {
          "Location_4_tm440000000": [
            "Mob_exp10_tm400",
            "Mob_exp15_tm400",
            "Mob_exp20_tm400"
          ]
        },
        {
          "Location_5_tm551000000": [
            {
              "Location_8_tm300000000": [
                "Mob_exp20_tm80020",
                "Mob_exp25_tm80025",
                "Mob_exp30_tm80030",
                "Mob_exp35_tm80035",
                "Mob_exp40_tm80040"
              ]
            },
            {
              "Location_9_tm300000000": [
                "Mob_exp30_tm30",
                {
                  "Location_11_tm40000000": [
                    "Boss_exp100_tm10400000",
                    {
                      "Location_B2_tm0.098765432": [
                        "Mob_exp40_tm50",
                        "Mob_exp40_tm50",
                        "Mob_exp40_tm50"
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "Location_6_tm300000000": [
            "Boss_exp280_tm10400000",
            {
              "Location_B1_tm0.098765432": [
                "Mob_exp10_tm0",
                "Mob_exp10_tm0",
                "Mob_exp10_tm0",
                "Mob_exp10_tm0",
                "Mob_exp10_tm0"
              ]
            }
          ]
        }
      ]
    }
  ]
}

class Warior:
    def __init__(self):
        self.exp = 0
        self.tm = Decimal('1234567890.0987654321')
        self.map = my_json

    def run(self):
        self.action(self.map)

    def action(self, map):
        chose = ''
        while chose != 'ВЫХОД':
            chose_list = self.down(map)
            act = int(input('введите номер выбранного варианта: '))
            chose = chose_list[act-1]
            if chose == 'ВЫХОД':
                self.up()
                break
            elif type(chose) == str:
                self.monstr_name_pars(chose)
                for key in map:
                    map[key].remove(chose)
            elif type(chose) == list:
                for monster in chose:
                    self.monstr_name_pars(monster)
                for key in map:
                    map[key].remove(chose)

            elif type(chose) == dict:
                for key in chose:
                    self.loc_name_pars(key)
                self.action(chose)
            else: print('XZXZXZZZZZZZZZZZZZZZZZZZ')
            cprint(f'exp - {self.exp}  ///  tm - {self.tm}', 'red')



    def down(self, up_level_dict):
        res = ''
        for key in up_level_dict:
            res = self.chose_variants(up_level_dict[key], up_level_dict)
        return res

    def up(self):
        print('-----^-----UP-----^-----')

    def chose_variants(self, level_list, map):
        cprint(f'exp - {self.exp}  ///  tm - {self.tm}', 'red')
        variant_str = '------выберите действие------\n'
        variant_list = []
        count = 1
        for elem in level_list:
            logging.debug(f'elem -->  {elem}')

            if type(elem) == str:
                variant_str += f'-{count}. убить монстра {elem}\n'
                variant_list.insert(count-1, elem)
                count += 1
                logging.info(f'elem str -->  {elem}')

            elif type(elem) == list:
                variant_str += f'-{count}. убить {len(elem)}х монстров: '
                variant_list.insert(count-1, elem)
                for sub_elem in elem:
                    variant_str += f'{sub_elem}, '
                variant_str += '\n'
                logging.info(f'elem list -->  {elem}')
                count += 1

            elif type(elem) == dict:
                for sub_key in elem:
                    sub_list = elem[sub_key]
                    variant_str += f'-{count}. спуститься в подземелье {sub_key}\n'
                    variant_list.insert(count-1, elem)  # sub_list
                    count += 1
                    logging.info(f'elem dict -->  {elem}')

        variant_str += f'-{count}. ВЫХОД'
        variant_list.insert(count - 1, 'ВЫХОД')
        cprint(variant_str, 'yellow')
        return variant_list

    def loc_name_pars(self, loc_name):
        regular = r'(Location_)(.+)(_tm)(\d+[.]?\d*)'
        match = re.search(regular, loc_name)
        # print(f'было времени  - {self.tm}')
        self.tm -= Decimal(match[4])
        # print(f'стало времени - {self.tm}')

    def monstr_name_pars(self, monstr_name):
        regular = r'(\w+_exp)(\d+)(_tm)(\d+[.]?\d*)'
        match = re.search(regular, monstr_name)
        # print(f'было опыта  - {self.exp}')
        self.exp += int(match[2])
        # print(f'стало опыта  - {self.exp}')
        # print(f'было времени  - {self.tm}')
        self.tm -= Decimal(match[4])
        # print(f'стало времени - {self.tm}')


logging.basicConfig(level=logging.ERROR)

warior = Warior()
warior.run()




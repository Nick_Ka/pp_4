# -*- coding: utf-8 -*-

# С помощью JSON файла rpg.json задана "карта" подземелья.
# Подземелье было выкопано монстрами и они всё ещё скрываются где-то в его глубинах,
# планируя набеги на близлежащие поселения.
# Само подземелье состоит из двух главных разветвлений и нескольких развилок,
# и лишь один из путей приведёт вас к главному Боссу
# и позволит предотвратить набеги и спасти мирных жителей.

# Напишите игру, в которой пользователь, с помощью консоли,
# сможет:
# 1) исследовать это подземелье:
#   -- передвижение должно осуществляться присваиванием переменной и только в одну сторону
#   -- перемещаясь из одной локации в другую, пользователь теряет время, указанное в конце названия каждой локации
# Так, перейдя в локацию Location_1_tm500 - вам необходимо будет списать со счёта 500 секунд.
# Тег, в названии локации, указывающий на время - 'tm'.
#
# 2) сражаться с монстрами:
#   -- сражение имитируется списанием со счета персонажа N-количества времени и получением N-количества опыта
#   -- опыт и время указаны в названиях монстров (после exp указано значение опыта и после tm указано время)
# Так, если в локации вы обнаружили монстра Mob_exp10_tm20 (или Boss_exp10_tm20)
# необходимо списать со счета 20 секунд и добавить 10 очков опыта.
# Теги указывающие на опыт и время - 'exp' и 'tm'.
# После того, как игра будет готова, сыграйте в неё и наберите 280 очков при положительном остатке времени.

# По мере продвижения вам так же необходимо вести журнал,
# в котором должна содержаться следующая информация:
# -- текущее положение
# -- текущее количество опыта
# -- текущая дата (отсчёт вести с первой локации с помощью datetime)
# После прохождения лабиринта, набора 280 очков опыта и проверки на остаток времени (remaining_time > 0),
# журнал необходимо записать в csv файл (назвать dungeon.csv, названия столбцов взять из field_names).

# Пример лога игры:
# Вы находитесь в Location_0_tm0
# У вас 0 опыта и осталось 1234567890.0987654321 секунд
# Прошло уже 0:00:00
# Внутри вы видите:
# -- Монстра Mob_exp10_tm0
# -- Вход в локацию: Location_1_tm10400000
# -- Вход в локацию: Location_2_tm333000000
# Выберите действие:
# 1.Атаковать монстра
# 2.Перейти в другую локацию
# 3.Выход
import csv
from datetime import datetime
from decimal import Decimal
import logging, json, re
from pprint import pprint

from termcolor import cprint

remaining_time = '1234567890.0987654321'
# если изначально не писать число в виде строки - теряется точность!
field_names = ['current_location', 'current_experience', 'current_date']


class Warior:
    def __init__(self, rt):
        self.location = "Location_0_tm0"
        self.tm = Decimal(rt)
        self.exp = 0
        self.map = None
        self.csv_table = [{'current_location': "Location_0_tm0",
                           'current_experience': 0, 'current_date': datetime.now()}]

    def run(self, file_name='rpg.json'):
        with open(file_name, 'r') as read_file:
            self.map = json.load(read_file)
        self.action(self.map)

    def action(self, map):
        chose = ''
        while chose != 'ВЫХОД':
            chose_list = self.down(map)
            act = int(input('введите номер выбранного варианта: '))
            chose = chose_list[act-1]
            if chose == 'ВЫХОД':
                self.up()
                break
            elif type(chose) == str:
                self.monstr_name_pars(chose)
                for key in map:
                    map[key].remove(chose)
            elif type(chose) == list:
                for monster in chose:
                    self.monstr_name_pars(monster)
                for key in map:
                    map[key].remove(chose)

            elif type(chose) == dict:
                for key in chose:
                    self.loc_name_pars(key)
                self.action(chose)
            else: print('XZXZXZZZZZZZZZZZZZZZZZZZ')

    def down(self, up_level_dict):
        res = ''
        for key in up_level_dict:
            self.csv_table.append({'current_location': key,
                                   'current_experience': self.exp, 'current_date': datetime.now()})
            res = self.chose_variants(up_level_dict[key])
        return res

    def up(self):
        print('-----^-----UP-----^-----')

    def win(self):
        cprint('I WIN', 'blue')
        # pprint(self.csv_table)
        with open('dungeon.csv', 'w', newline='') as csv_file:
            writer = csv.DictWriter(csv_file, delimiter=',', fieldnames=field_names)
            writer.writeheader()
            writer.writerows(self.csv_table)

    def chose_variants(self, level_list):
        cprint(f'exp - {self.exp}  ///  tm - {self.tm}', 'red')
        if self.exp >= 280 and self.tm >= 0:
            self.win()
        variant_str = '------выберите действие------\n'
        variant_list = []
        count = 1
        for elem in level_list:
            logging.debug(f'elem -->  {elem}')

            if type(elem) == str:
                variant_str += f'-{count}. убить монстра {elem}\n'
                variant_list.insert(count-1, elem)
                count += 1
                logging.info(f'elem str -->  {elem}')

            elif type(elem) == list:
                variant_str += f'-{count}. убить {len(elem)}х монстров: '
                variant_list.insert(count-1, elem)
                for sub_elem in elem:
                    variant_str += f'{sub_elem}, '
                variant_str += '\n'
                logging.info(f'elem list -->  {elem}')
                count += 1

            elif type(elem) == dict:
                for sub_key in elem:
                    sub_list = elem[sub_key]
                    variant_str += f'-{count}. спуститься в подземелье {sub_key}\n'
                    variant_list.insert(count-1, elem)  # sub_list
                    count += 1
                    logging.info(f'elem dict -->  {elem}')

        variant_str += f'-{count}. ВЫХОД'
        variant_list.insert(count - 1, 'ВЫХОД')
        cprint(variant_str, 'yellow')
        return variant_list

    def loc_name_pars(self, loc_name):
        regular = r'(Location_)(.+)(_tm)(\d+[.]?\d*)'
        match = re.search(regular, loc_name)
        # print(f'было времени  - {self.tm}')
        self.tm -= Decimal(match[4])
        # print(f'стало времени - {self.tm}')

    def monstr_name_pars(self, monstr_name):
        regular = r'(\w+_exp)(\d+)(_tm)(\d+[.]?\d*)'
        match = re.search(regular, monstr_name)
        # print(f'было опыта  - {self.exp}')
        self.exp += int(match[2])
        # print(f'стало опыта  - {self.exp}')
        # print(f'было времени  - {self.tm}')
        self.tm -= Decimal(match[4])
        # print(f'стало времени - {self.tm}')


logging.basicConfig(level=logging.ERROR)


if __name__ == '__main__':
    warior = Warior(remaining_time)
    warior.run()












# Учитывая время и опыт, не забывайте о точности вычислений!


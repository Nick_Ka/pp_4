from random import randint
from termcolor import cprint


class Man:
    def __init__(self, name):
        self.name = name
        self.fullness = 50
        self.house = None

    def __str__(self):
        return 'Я - {}, сытость - {}'.format(self.name, self.fullness)

    def eat(self):
        self.house.food -= 10
        self.fullness += 10
        cprint('{} - поел'.format(self.name), color="green")

    def shopping(self):
        self.house.food += 50
        self.house.money -= 50
        cprint('{} - сходил в магазин'.format(self.name), color="green")

    def work(self):
        self.fullness -= 10
        self.house.money += 50
        cprint('{} - заработал бабла'.format(self.name), color="green")

    def wasting_time(self):
        self.fullness -= 10
        cprint('{} - пинал балду целый день!!!!!!!!!!!'.format(self.name), color="blue")

    def check_in(self, house):
        self.house = house
        cprint('{} - заселился в дом!!!'.format(self.name), color="grey")
        self.fullness -= 10

    def act(self):
        if self.house.money <= 10:
            self.work()
        elif self.house.food <= 10:
            self.shopping()
        else:
            dice = randint(1, 6)
            if dice == 1:
                self.work()
            elif dice == 2:
                self.eat()
            elif dice == 3:
                self.shopping()
            else:
                self.wasting_time()

        if self.fullness <= 10:
            self.eat()

        if self.fullness <= 0:
            cprint('Кончился {}, пусть земля ему будет пухом :('.format(self.name), color='red')


class House:
    def __init__(self):
        self.food = 50
        self.money = 50

    def __str__(self):
        return 'В доме еды осталось - {}, денег осталось - {}'.format(self.food, self.money)
class Cat:
    def __init__(self):
        self.fullness = 5
        self.house = None

    def eat(self):
        self.fullness += 5
        self.house.food -= 5
        cprint('кот поел', color='grey')

    def play(self):
        self.fullness -= 1
        cprint('кот поиграл', color='grey')

    def sleep(self):
        self.fullness -= 1
        cprint('кот поспал', color='grey')

    def bay_cat(self, house):
        self.house = house
        cprint('Мы купили кота!!!', color="grey")

    def act(self):
        dice = randint(1, 3)
        if self.fullness <= 1:
            self.eat()
        elif dice == 1:
            self.play()
        else:
            self.sleep()


citizens = [
    Man(name='Бивис'),
    Man(name='Батхед'),
    Man(name='Кенни'),

]

bb_house = House()
for citizen in citizens:
    citizen.check_in(house=bb_house)


for day in range(1, 366):
    print('-------------день - {}---------------'.format(day))
    for citizen in citizens:
        citizen.act()
        cprint(citizen, color='magenta')
    if bb_house.money > 500:
        pass
        ########################################################
    print(bb_house)

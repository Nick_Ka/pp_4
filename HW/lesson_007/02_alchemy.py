# -*- coding: utf-8 -*-

# Создать прототип игры Алхимия: при соединении двух элементов получается новый.
# Реализовать следующие элементы: Вода, Воздух, Огонь, Земля, Шторм, Пар, Грязь, Молния, Пыль, Лава.
# Каждый элемент организовать как отдельный класс.
# Таблица преобразований:
#   Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава

# Сложение элементов реализовывать через __add__
# Если результат не определен - то возвращать None
# Вывод элемента на консоль реализовывать через __str__
#
# Примеры преобразований:
#   print(Water(), '+', Air(), '=', Water() + Air())
#   print(Fire(), '+', Air(), '=', Fire() + Air())

class Water:
    def __init__(self):
        self.elem = None

    def __str__(self):
        return 'Вода'

    def __add__(self, elem):
        self.elem = elem
        if isinstance(self.elem, Air):
            return Storm()
        elif isinstance(self.elem, Fire):
            return Steem()
        elif isinstance(self.elem, Ground):
            return Mud()
        elif isinstance(self.elem, Movement):
            return Tsunami()
        else:
            return self

class Air:
    def __init__(self):
        self.elem = None

    def __str__(self):
        return 'Воздух'

    def __add__(self, elem):
        self.elem = elem
        if isinstance(self.elem, Water):
            return Storm()
        elif isinstance(self.elem, Fire):
            return Lightning()
        elif isinstance(self.elem, Ground):
            return Dust()
        elif isinstance(self.elem, Movement):
            return Hurricane()
        else:
            return self

class Fire:
    def __init__(self):
        self.elem = None
        
    def __str__(self):
        return 'Огонь'

    def __add__(self, elem):
        self.elem = elem
        if isinstance(self.elem, Water):
            return Steem()
        elif isinstance(self.elem, Air):
            return Lightning()
        elif isinstance(self.elem, Ground):
            return Lava()
        elif isinstance(self.elem, Movement):
            return Forest_fire()
        else:
            return self
        
class Ground:
    def __init__(self):
        self.elem = None
        
    def __str__(self):
        return 'Земля'

    def __add__(self, elem):
        self.elem = elem
        if isinstance(self.elem, Water):
            return Mud()
        elif isinstance(self.elem, Fire):
            return Lava()
        elif isinstance(self.elem, Air):
            return Dust()
        elif isinstance(self.elem, Movement):
            return Earthquake()
        else:
            return self
class Movement:
    def __init__(self):
        self.elem = None
        
    def __str__(self):
        return 'Движение'

    def __add__(self, elem):
        self.elem = elem
        if isinstance(self.elem, Water):
            return Tsunami()
        elif isinstance(self.elem, Fire):
            return Forest_fire()
        elif isinstance(self.elem, Air):
            return Hurricane()
        elif isinstance(self.elem, Ground):
            return Earthquake()
        else:
            return self

        
class Storm:
    def __str__(self):
        return 'Шторм'
class Steem:
    def __str__(self):
        return 'Пар'
class Mud:
    def __str__(self):
        return 'Грязь'   
class Lightning:
    def __str__(self):
        return 'Молния' 
class Dust:
    def __str__(self):
        return 'Пыль' 
class Lava:
    def __str__(self):
        return 'Лава'
    
class Tsunami:
    def __str__(self):
        return 'Цунами' 
class Forest_fire:
    def __str__(self):
        return 'Пожар' 
class Hurricane:
    def __str__(self):
        return 'Ураган' 
class Earthquake:
    def __str__(self):
        return 'Землетрясение'

elements = [Water(), Air(), Fire(), Ground(), Movement()]
for element in elements:
    for element_2 in elements:
        print(element, '+', element_2, '=', element + element_2)
        print("---------************----------")

    
        



# Усложненное задание (делать по желанию)
# Добавить еще элемент в игру.
# Придумать что будет при сложении существующих элементов с новым.

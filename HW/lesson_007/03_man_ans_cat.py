# -*- coding: utf-8 -*-

from random import randint

# Доработать практическую часть урока lesson_007/python_snippets/08_practice.py

# Необходимо создать класс кота. У кота есть аттрибуты - сытость и дом (в котором он живет).
# Кот живет с человеком в доме.
# Для кота дом характеризируется - миской для еды и грязью.
# Изначально в доме нет еды для кота и нет грязи.

# Доработать класс человека, добавив методы
#   подобрать кота - у кота появляется дом.
#   купить коту еды - кошачья еда в доме увеличивается на 50, деньги уменьшаются на 50.
#   убраться в доме - степень грязи в доме уменьшается на 100, сытость у человека уменьшается на 20.
# Увеличить кол-во зарабатываемых человеком денег до 150 (он выучил пайтон и устроился на хорошую работу :)

# Кот может есть, спать и драть обои - необходимо реализовать соответствующие методы.
# Когда кот спит - сытость уменьшается на 10
# Когда кот ест - сытость увеличивается на 20, кошачья еда в доме уменьшается на 10.
# Когда кот дерет обои - сытость уменьшается на 10, степень грязи в доме увеличивается на 5
# Если степень сытости < 0, кот умирает.
# Так же надо реализовать метод "действуй" для кота, в котором он принимает решение
# что будет делать сегодня

# Человеку и коту надо вместе прожить 365 дней.

from random import randint
from termcolor import cprint


class House:
    def __init__(self):
        self.food = 50
        self.money = 50

        self.cat_food = 0
        self.cat_mud = 0

    def __str__(self):
        return 'В доме еды осталось - {}, денег осталось - {},' \
               ' Кошачей еды осталось - {}, грязи от кота - {}, в доме живет'.format(self.food, self.money,
                                                                                                  self.cat_food,
                                                                                                  self.cat_mud)


class Cat:
    def __init__(self):
        self.fullness = 5
        self.house = None

    def __str__(self):
        return 'я кот. моя сытость - {}'.format(self.fullness)

    def eat(self):
        self.fullness += 5
        self.house.cat_food -= 5
        cprint('кот поел', color='blue')

    def play(self):
        self.fullness -= 1
        self.house.cat_mud += 10
        cprint('кот поиграл, все засрал!!!', color='blue')

    def sleep(self):
        self.fullness -= 1
        cprint('кот поспал', color='blue')

    def act(self):
        dice = randint(1, 3)
        if self.fullness <= 1:
            self.eat()
        elif dice == 1:
            self.sleep()
        else:
            self.play()


class Man:
    def __init__(self, name):
        self.name = name
        self.fullness = 50
        self.house = None
        self.cat = None

    def __str__(self):
        return 'Я - {}, сытость - {}'.format(self.name, self.fullness)

    def eat(self):
        self.house.food -= 20
        self.fullness += 20
        cprint('{} - поел'.format(self.name), color="green")

    def shopping(self):
        self.house.food += 50
        self.house.money -= 50
        cprint('{} - сходил в магазин'.format(self.name), color="green")

    def work(self):
        self.fullness -= 10
        self.house.money += 150
        cprint('{} - заработал бабла'.format(self.name), color="green")

    def wasting_time(self):
        self.fullness -= 10
        cprint('{} - пинал балду целый день!!!!!!!!!!!'.format(self.name), color="green")

    def check_in(self, house):
        self.house = house
        cprint('{} - заселился в дом!!!'.format(self.name), color="green")
        self.fullness -= 10

    def take_cat(self, house):
        self.cat = Cat()
        self.cat.house = house
        cprint('{} - подобрал кота!!!'.format(self.name), color="grey")

    def by_cat_food(self):
        self.house.money -= 50
        self.house.cat_food += 50
        self.fullness -= 10
        cprint('{} - купил еды коту :( '.format(self.name), color='grey')

    def clean_house(self):
        self.house.cat_mud -= 20
        self.fullness -= 20
        cprint('{} - убрал за котом :( '.format(self.name), color='grey')

    def take_care_of_cat(self):
        if self.house.cat_mud >= 50:
            self.clean_house()
        elif self.cat.fullness <= 0:
            cprint('кот сдох с голоду. печалька......', color='red')
        elif self.house.cat_food <= 20:
            self.by_cat_food()
        else:
            self.wasting_time()

    def act(self):
        if self.house.money <= 10:
            self.work()
        elif self.house.food <= 10:
            self.shopping()
        else:
            dice = randint(1, 6)
            if dice == 1:
                self.work()
            elif dice == 2:
                self.eat()
            elif dice == 3:
                self.shopping()
            else:
                self.wasting_time()

        if self.fullness <= 30:
            self.eat()

        if self.fullness <= 0:
            cprint('Кончился {}, пусть земля ему будет пухом :('.format(self.name), color='red')


citizens = [
    Man(name='Бивис'),
    Man(name='Батхед'),
    Man(name='Кенни'),
]
bb_house = House()

for citizen in citizens:
    citizen.check_in(house=bb_house)

citizens[0].take_cat(house=bb_house)

for day in range(1, 366):
    print('-------------день - {}---------------'.format(day))
    for citizen in citizens:

        if citizen.cat is not None:
            citizen.cat.act()
            citizen.take_care_of_cat()
            citizen.act()
            cprint(citizen.cat, color='blue')
        else:
            citizen.act()

        cprint(citizen, color='magenta')
    print(bb_house)

# Усложненное задание (делать по желанию)
# Создать несколько (2-3) котов и подселить их в дом к человеку.
# Им всем вместе так же надо прожить 365 дней.

# (Можно определить критическое количество котов, которое может прокормить человек...)

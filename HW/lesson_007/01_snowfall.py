# -*- coding: utf-8 -*-

import simple_draw as sd
from random import randint


# Шаг 1: Реализовать падение снежинки через класс. Внести в методы:
#  - создание снежинки с нужными параметрами
#  - отработку изменений координат
#  - отрисовку


class Snowflake:
    def __init__(self, x=100, y=600, length=50, color=sd.COLOR_WHITE,
                 factor_a=0.5, factor_b=0.5, factor_c=50):
        self.x = x
        self.y = y
        self.length = length
        self.color = color
        self.factor_a = factor_a
        self.factor_b = factor_b
        self.factor_c = factor_c

    def __str__(self):
        return 'координаты - ({}, {})'.format(self.x, self.y)

    def clear_previous_picture(self):
        start_point = sd.get_point(self.x, self.y)
        sd.snowflake(center=start_point, length=self.length,
                     factor_a=self.factor_a, factor_b=self.factor_b, factor_c=self.factor_c,
                     color=sd.background_color)

    def move(self):
        self.x = self.x + randint(-20, 20)
        self.y = self.y + randint(-40, 00)

    def draw(self):
        start_point = sd.get_point(self.x, self.y)
        sd.snowflake(center=start_point, length=self.length,
                     factor_a=self.factor_a, factor_b=self.factor_b, factor_c=self.factor_c,
                     color=self.color)

    def can_fall(self):
        if self.y <= 0:
            print('self.y = ', self.y)
            return False
        else:
            return True


# flake = Snowflake()
#
# while True:
#     flake.clear_previous_picture()
#     flake.move()
#     flake.draw()
#     if not flake.can_fall():
#         break
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# шаг 2: создать снегопад - список объектов Снежинка в отдельном списке, обработку примерно так:
def get_flakes(count=20):
    flakes = []
    for i in range(0, count):
        flakes.append(Snowflake(x=randint(0, 550), y=randint(100, 600), length=randint(10, 30),
                                color=(randint(0, 255), randint(0, 255), randint(0, 255)), factor_a=randint(45, 65) * 0.01, factor_b=randint(45, 65) * 0.01,
                                factor_c=randint(45, 65)))
    return flakes


def get_fallen_flakes():
    fallen_flakes = 0
    for flake in flakes:
        if not flake.can_fall():
            print('удаляем снежинку - ', flake)
            flakes.remove(flake)

            fallen_flakes += 1

    return fallen_flakes


def append_flakes(count):
    for flake in range(0, count):
        flakes.append(Snowflake(x=randint(0, 550), y=randint(200, 600), length=randint(10, 30),
                                color=(randint(0, 255), randint(0, 255), randint(0, 255)),
                                factor_a=randint(45, 65) * 0.01, factor_b=randint(45, 65) * 0.01,
                                factor_c=randint(45, 65)))


N = 30
flakes = get_flakes(count=N)  # создать список снежинок
while True:
    for flake in flakes:
        flake.clear_previous_picture()
        flake.move()
        flake.draw()

    fallen_flakes = get_fallen_flakes()  # подчитать сколько снежинок уже упало
    if fallen_flakes:
        append_flakes(count=fallen_flakes)  # добавить еще сверху
    sd.sleep(0.1)
    if sd.user_want_exit():
        break

sd.pause()

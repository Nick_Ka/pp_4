# -*- coding: utf-8 -*-

# Есть файл с протоколом регистраций пользователей на сайте - registrations.txt
# Каждая строка содержит: ИМЯ ЕМЕЙЛ ВОЗРАСТ, разделенные пробелами
# Например:
# Василий test@test.ru 27
#
# Надо проверить данные из файла, для каждой строки:
# - присутсвуют все три поля
# - поле имени содержит только буквы
# - поле емейл содержит @ и .
# - поле возраст является числом от 10 до 99
#
# В результате проверки нужно сформировать два файла
# - registrations_good.log для правильных данных, записывать строки как есть
# - registrations_bad.log для ошибочных, записывать строку и вид ошибки.
#
# Для валидации строки данных написать метод, который может выкидывать исключения:
# - НЕ присутсвуют все три поля: ValueError
# - поле имени содержит НЕ только буквы: NotNameError (кастомное исключение)
# - поле емейл НЕ содержит @ и .(точку): NotEmailError (кастомное исключение)
# - поле возраст НЕ является числом от 10 до 99: ValueError
# Вызов метода обернуть в try-except.
import datetime



class NotNameError(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return 'некорректный ввод имени'


class NotEmailError(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return 'некорректный ввод почты'


class NotAgeError(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return 'некорректный ввод возраста '


class FileParser:
    def __init__(self, input_file):
        self.input_file = input_file
        self.reg_good_file = 'registrations_good.log'
        self.reg_bad_file = 'registrations_bad.log'
        self.line = ''

    def valid_line(self):

        sp_line = self.line.split(' ')

        name = sp_line[0]
        e_mail = sp_line[1]
        age = int(sp_line[2])

        if len(sp_line) > 3:
            raise ValueError
        if not name.isalpha():
            raise NotNameError
        if not (('@' in e_mail) and ('.' in e_mail)):
            raise NotEmailError
        if age > 99 or age < 10:
            raise NotAgeError

    def registrations_good(self):
        with open(self.reg_good_file, 'a') as file:
            file.write(self.line)

    def registrations_bad(self, exc):
        with open(self.reg_bad_file, 'a') as file:
            self.line = self.line[:-1]

            st = f'{self.line: <40}| {exc}\n'
            file.write(st)

    def parse_data(self):
        now = datetime.datetime.now()
        with open(self.reg_good_file, 'w') as file:
            file.write(f'создан log-file со списком корректных данных\nдата: {now.strftime("%d-%m-%Y %H:%M")}\n\n')
        with open(self.reg_bad_file, 'w') as file:
            file.write(f'создан log-file со списком НЕкорректных данных\nдата: {now.strftime("%d-%m-%Y %H:%M")}\n\n')


        with open(self.input_file, 'r', encoding='utf8') as file:
            for line in file:
                try:
                    self.line = line
                    self.valid_line()
                except (ValueError, IndexError, NotAgeError, NotNameError, NotEmailError) as exc:
                    self.registrations_bad(exc=exc)
                else:
                    self.registrations_good()


obj = FileParser(input_file='registrations.txt')
obj.parse_data()

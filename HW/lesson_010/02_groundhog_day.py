# -*- coding: utf-8 -*-

# День сурка
#
# Напишите функцию one_day() которая возвращает количество кармы от 1 до 7
# и может выкидывать исключения:
# - IamGodError
# - DrunkError
# - CarCrashError
# - GluttonyError
# - DepressionError
# - SuicideError
# Одно из этих исключений выбрасывается с вероятностью 1 к 13 каждый день
#
# Функцию оберните в бесконечный цикл, выход из которого возможен только при накоплении
# кармы до уровня ENLIGHTENMENT_CARMA_LEVEL. Исключения обработать и записать в лог.
# При создании собственных исключений максимально использовать функциональность
# базовых встроенных исключений.
import random
from random import randint


class IamGodError(Exception):
    def __init__(self, day):
        self.day = day

    def __str__(self):
        return 'по моему я БОГ!!!!!'


class DrunkError(Exception):
    def __init__(self, day):
        self.day = day

    def __str__(self):
        return 'чёто я перебрал....'


class CarCrashError(Exception):
    def __init__(self, day):
       self.day = day

    def __str__(self):
        return 'тачка в смятку, а я в тачке'


class GluttonyError(Exception):
    def __init__(self, day):
        self.day = day

    def __str__(self):
        return 'похоже я кнчился :('


class DepressionError(Exception):
    def __init__(self, day):
        self.day = day

    def __str__(self):
        return 'дипресняяяяяк'


class SuicideError(Exception):
    def __init__(self, day):
        self.day = day

    def __str__(self):
        return 'похоже я кнчился :('


def one_day(day_co, carma_lev):
    dice = randint(1, 13)
    if dice == 5:
        exc = random.choice(exc_list)(day_co)
        raise exc
    else:
        carma_lev += randint(1, 7)
    return carma_lev

def print_to_file(ex, day):
    with open('logs.txt', 'a', encoding='utf8') as file:
        ex = str(ex)
        file.write(f'|ошибка: {ex: <30}| день - {day: ^5}|\n')


ENLIGHTENMENT_CARMA_LEVEL = 777
exc_list = [SuicideError, DepressionError, GluttonyError, CarCrashError, DrunkError, IamGodError]

carma_level = 0
day_count = 1
with open('logs.txt', 'w', encoding='utf8') as file:
    file.write('Начинаем день сурка\n \n')

while carma_level < ENLIGHTENMENT_CARMA_LEVEL:
    try:
        carma_level = one_day(day_co=day_count, carma_lev=carma_level)
    except SuicideError as exc:
        print_to_file(ex=exc, day=day_count)
    except DepressionError as exc:
        print_to_file(ex=exc, day=day_count)
    except GluttonyError as exc:
        print_to_file(ex=exc, day=day_count)
    except CarCrashError as exc:
        print_to_file(ex=exc, day=day_count)
    except DrunkError as exc:
        print_to_file(ex=exc, day=day_count)
    except IamGodError as exc:
        print_to_file(ex=exc, day=day_count)

    day_count += 1

# https://goo.gl/JnsDqu
